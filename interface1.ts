interface Rectangle {
    height: number,
    width: number
  }

interface ColoredRectangle extends Rectangle {
    color: string
}

const rectangle: Rectangle = {
    height: 20,
    width: 10
};

const coloredRectangle: ColoredRectangle = {
    height: 20,
    width: 10,
    color: "red"
};

console.log(rectangle);
console.log(coloredRectangle);